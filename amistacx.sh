#sudo redis-cli flushall
sudo php bin/magento cache:clean
sudo php bin/magento cache:disable
sudo chown -R www-data:www-data /var/www/magento
sudo php bin/magento setup:upgrade
sudo chmod -R u+rwX,go+rX,go-w /var/www/magento
sudo chown -R www-data:www-data /var/www/magento
sudo php bin/magento setup:di:compile
sudo chown -R www-data:www-data /var/www/magento
sudo php bin/magento setup:static-content:deploy -f
sudo chown -R www-data:www-data /var/www/magento
sudo php bin/magento cache:clean
sudo php bin/magento cache:enable
sudo chown -R www-data:www-data /var/www/magento